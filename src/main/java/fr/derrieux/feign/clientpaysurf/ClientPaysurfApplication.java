package fr.derrieux.feign.clientpaysurf;

import java.io.IOException;
import java.net.http.HttpHeaders;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RestController;

import fr.derrieux.feign.clientpaysurf.Client.MangopayClient;
import fr.derrieux.feign.clientpaysurf.Client.PaysurfClient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mangopay.MangoPayApi;
import com.mangopay.entities.KycDocument;
import com.mangopay.entities.User;
import com.mangopay.entities.UserLegal;
// import com.mangopay.entities.UserLegal;
import com.mangopay.core.Configuration;
import com.mangopay.core.APIs.ClientApi;
import com.mangopay.core.APIs.UserApi;



@SpringBootApplication
@EnableFeignClients
@RestController
public class ClientPaysurfApplication {

	@Autowired
	private PaysurfClient paysurf;
	
	@Autowired
	private MangopayClient mangopay;
	
	@Autowired
	private HttpServletRequest context;
	public static void main(String[] args) {

		SpringApplication.run(ClientPaysurfApplication.class, args);
		
	}
	
	@GetMapping(value = "/test2")
	public String test2() throws IOException {
		System.out.println("/test2");

		return ("/test2");
	}

    @RequestMapping("/test")
    public String forward() {
		System.out.println("test");
        return "forward:/test2";
	}
	
	// protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
	// 	RequestDispatcher dispatcher = req.getServletContext()
	// 	  .getRequestDispatcher("/test2");
	// 	dispatcher.forward(req, resp);
	// }



	@GetMapping(value = "/")
	public String getPaysurf() throws IOException {
		System.out.println("root");

		return ("hi");
	}

	@GetMapping(value = "/v2/comitidev/users/99636216")
	public String testRoot() throws IOException {
		System.out.println("/v2/comitidev/users/99636216");

		return ("/v2/comitidev/users/99636216");
	}

	@GetMapping(value = "/testClientMangopay")
	public String getTokken() throws IOException {
		String token = this.mangopay.PostToken("{}");
		return token;
	}

	@GetMapping(value = "/testClientPaysurf")
	public String postAccount() throws IOException {
		String n = "34";
		String json = 
			"{"
				+ "'accountId': 'Jon" + n + "',"
				+ "'email': 'Wilhelm35" + n + "@gmail.com',"
				+ "'title': 'M',"
				+ "'firstName': 'Sammie',"
				+ "'lastName': 'Rohan',"
				+ "'birthName': 'Hayes',"
				+ "'mobilePhone': '0381192161',"
				+ "'extraPhone': '0381192161',"
				+ "'street': '65089 Prudence Motorway',"
				+ "'postCode': '03213',"
				+ "'city' : 'South Linnie',"
				+ "'country': 'FRA',"
				+ "'registrationIp': '1.2.3.4',"
				+ "'birthDate': '1990-07-07',"
				+ "'companyName': 'revolutionize2',"
				+ "'companyWebsite': 'http://edgar2.org',"
				+ "'companyDescription': 'RAM Nebraska Islands Kroon indigo',"
				+ "'nationality': 'FRA',"
				+ "'birthCity': 'Jadenmouth',"
				+ "'birthCountry' : 'FRA',"
				+ "'registrationNumber' : 'copy Steel Research payment digital" + n + "',"
				+ "'legalForm' : 'SA'"
			+ "}";
		String user = this.paysurf.postAccounts(json);
		return user;
	}
}
