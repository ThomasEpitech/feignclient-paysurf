package fr.derrieux.feign.clientpaysurf.Controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;


public class MangopayController {

@GetMapping(value = "/users/legal")
	public String MangoGetUsersLegal() throws IOException {
		return "Mango Forward MangoGetUsersLegal";
    }

@PutMapping(value = "/users/legal/:UserId")
	public String MangoGetUserLegal() throws IOException {
		return "Mango Forward MangoGetUserLegal";
    }
@GetMapping(value = "/users/natural")
    public String MangoGetUsersNatural() throws IOException {
        return "Mango Forward MangoGetUsersNatural";
    }
@PutMapping(value = "/users/natural/:UserId")
	public String MangoGetUserNatural() throws IOException {
		return "Mango Forward MangoGetUserNatural";
    }
    
@GetMapping(value = "/users/:UserId")
    public String MangoGetUser() throws IOException {
        return "Mango Forward MangoGetUser";
    }

@GetMapping(value = "/wallets/:WalletId")
    public String MangoGetWallet() throws IOException {
        return "Mango Forward MangoGetWallet";
    }

@PostMapping(value = "/payins/card/web/")
    public String MangoPostPayinCardWeb() throws IOException {
        return "Mango Forward MangoPostPayinCardWeb";
    }

@GetMapping(value = "/payins/:PayInId/")
    public String MangoGetPayin() throws IOException {
        return "Mango Forward MangoGetPayin";
    }

@PostMapping(value = "/payins/bankwire/direct/")
    public String MangoPostPayinBankwireDirect() throws IOException {
        return "Mango Forward MangoPostPayinBankwireDirect";
    }

@PostMapping(value = "/payouts/bankwire/")
    public String MangoPostPayoutBankwire() throws IOException {
        return "Mango Forward MangoPostPayoutBankwire";
    }

@PostMapping(value = "/transfers/")
    public String MangoPostTransfer() throws IOException {
        return "Mango Forward MangoPostTransfer";
    }

@GetMapping(value = "/transfers/:TransferId")
    public String MangoGetTransfer() throws IOException {
        return "Mango Forward MangoGetTransfer";
    }

@GetMapping(value = "/kyc/documents/:KYCDocumentId/")
    public String MangoGetKycDocument() throws IOException {
        return "Mango Forward MangoGetKycDocument";
    }

@PostMapping(value = "/users/:UserId/kyc/documents/")
    public String MangoPostKycDocument() throws IOException {
        return "Mango Forward MangoPostKycDocument";
    }

@PostMapping(value = "/users/:UserId/kyc/documents/:KYCDocumentId/pages/")
    public String MangoPostKycDocumentPage() throws IOException {
        return "Mango Forward MangoPostKycDocumentPage";
    }

@PutMapping(value = "/users/:UserId/kyc/documents/:KYCDocumentId ")
    public String MangoPutKycDocument() throws IOException {
        return "Mango Forward MangoPutKycDocument";
    }

@GetMapping(value = "/ClientId/users/UserId/bankaccounts/BankAccountId")
    public String MangoGetUserBankAccount() throws IOException {
        return "Mango Forward MangoGetUserBankAccount";
    }

@PostMapping(value = "/cardregistrations")
    public String MangoPostCardRegistration() throws IOException {
        return "Mango Forward MangoPostCardRegistration";
    }

@PostMapping(value = "/reports/wallets/")
    public String MangoPostReportWallet() throws IOException {
        return "Mango Forward MangoPostCardRegistration";
    }






















































































}