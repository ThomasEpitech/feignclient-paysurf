package fr.derrieux.feign.clientpaysurf.Client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import fr.derrieux.feign.clientpaysurf.Config.MangopayFeignConfig;
// import fr.derrieux.feign.clientpaysurf.Model.PaysurfUser;
// import fr.derrieux.feign.clientpaysurf.Model.User;

/**
 * Bean permettant d'emmetre des requêtes sur l'api de paysurf
 * Ajouter dans l'annotation @Headers la liste des en-têtes génériques
 */
@FeignClient(
    name = "mangopay" , 
    url = "${mangopay.api.base-url}",
    configuration = MangopayFeignConfig.class)
@RequestMapping(headers = {
        // "x-api-key=${mangopay.api.headers.x-api-key}",
        // "ps-ip-address=${mangopay.api.headers.ps-ip-address}",
        "Connection=keep-alive",
        "Content-Type=application/x-www-form-urlencoded",
        "Authorization=Basic Y29taXRpZGV2OjgzMTR0c04wYjVFRVZrUW9UZUs5d2sxVVNQTUN0NjlFUmZycWpZWjFkWm1adG1oaE1h"
})
public interface MangopayClient {

    @PostMapping(
        value="/v2.01/oauth/token/")
    public String PostToken(String json);

}
