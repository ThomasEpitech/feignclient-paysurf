// package fr.derrieux.feign.clientpaysurf.Model;

// import javax.persistence.CascadeType;
// import javax.persistence.Entity;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
// import javax.persistence.JoinColumn;
// import javax.persistence.ManyToOne;
// import javax.persistence.OneToOne;

// import lombok.Data;

// @Data
// public class MangopayLegalUser extends MangopayUser {
//     private MangopayAddress HeadquartersAddress;
//     private String LegalPersonType;
//     private String Name;
//     private MangopayAddress LegalRepresentativeAddress;
//     private String LegalRepresentativeBirthday;
//     private String LegalRepresentativeCountryOfResidence;
//     private String LegalRepresentativeNationality;
//     private String LegalRepresentativeEmail;
//     private String LegalRepresentativeFirstName;
//     private String LegalRepresentativeLastName;
//     private String LegalRepresentativeProofOfIdentity;
//     private String Statute;
//     private String ShareholderDeclaration;
//     private String ProofOfRegistration;
//     private String CompanyNumber;
// }
